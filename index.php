<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laba1</title>
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/reseter.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<?php echo'
    <body>
        <header class="header">
            <img src="assets\images\polykek.png" alt="polytech" class="logo">
            <h1 class="name">Задание №1 "Hello World!"</h1>
        </header>
        <main class="main">
            <h2 class="hello-world">Hellow World!</h2>
        </main>
        <footer class="footer">
            <p class="author">Крапивин Иван Сергеевич 211-321</p>
        </footer>
    </body>
'?>
</html>